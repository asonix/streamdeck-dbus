use std::{
    collections::{HashMap, HashSet},
    sync::Arc,
    time::Duration,
};
use tokio::sync::RwLock;
use tokio_actors::{Actor, Context, Handle, SendHandle};
use zbus::SignalContext;

mod connector;
mod deck;
mod searcher;

use connector::Connector;
use deck::{Deck, DeckMessage};
use searcher::Searcher;

#[derive(Clone, Debug)]
enum ManagerMessage {
    Found(String),
    Removed(String),
    Connected(Deck),
    Ignored(String),
}

#[derive(Debug)]
struct ConnectedDeck {
    object_name: String,
    handle: SendHandle<DeckMessage>,
}

#[derive(Debug, Default)]
struct Inner {
    pending_ports: HashSet<String>,
    connected_decks: HashMap<String, ConnectedDeck>,
    ignored_ports: HashSet<String>,
    retrying_ports: HashSet<String>,
    children: HashMap<usize, String>,
}

#[derive(Clone)]
pub(super) struct Manager {
    connection: zbus::Connection,
    inner: Arc<RwLock<Inner>>,
}

impl std::fmt::Debug for Manager {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Manager")
            .field("inner", &self.inner)
            .finish()
    }
}

#[zbus::dbus_interface(name = "dog.asonix.DeckManager1")]
impl Manager {
    #[dbus_interface(property, name = "ConnectedDecks")]
    async fn connected_decks(&self) -> Vec<String> {
        let mut vec: Vec<String> = self
            .inner
            .read()
            .await
            .connected_decks
            .values()
            .map(|connected| connected.object_name.clone())
            .collect();

        vec.sort();
        vec
    }
}

impl Manager {
    pub(super) async fn start(root_handle: &mut Handle<()>) -> tokio_actors::Result<()> {
        let connection = zbus::Connection::session().await?;
        connection.request_name("dog.asonix.DeckManager").await?;

        let manager = Manager {
            connection: connection.clone(),
            inner: Arc::new(RwLock::new(Inner::default())),
        };

        connection
            .object_server_mut()
            .await
            .at(manager.object_name(), manager.clone())?;

        let manager = root_handle
            .spawn_child_with_hooks(
                Actor::new(manager, Manager::turn).on_remove(Manager::on_remove),
            )
            .await?;
        let searcher = Searcher::new(manager);
        let searcher = root_handle.spawn_child(searcher, Searcher::turn).await?;
        searcher.every(Duration::from_secs(1), || ());

        tracing::debug!("new manager");
        Ok(())
    }

    #[tracing::instrument(skip(ctx))]
    async fn turn(
        &mut self,
        msg: ManagerMessage,
        ctx: &mut Context<ManagerMessage>,
    ) -> tokio_actors::Result<()> {
        match msg {
            ManagerMessage::Found(port_name) => self.found(port_name, ctx).await,
            ManagerMessage::Removed(port_name) => self.removed(port_name).await,
            ManagerMessage::Connected(deck) => self.connected(deck, ctx).await,
            ManagerMessage::Ignored(port_name) => self.ignored(port_name).await,
        }
    }

    #[tracing::instrument(skip(ctx))]
    async fn on_remove(
        &mut self,
        child_id: usize,
        ctx: &mut Context<ManagerMessage>,
    ) -> tokio_actors::Result<()> {
        let mut inner = self.inner.write().await;

        if let Some(port_name) = inner.children.remove(&child_id) {
            inner.connected_decks.remove(&port_name);
            inner.retrying_ports.insert(port_name.clone());
            ctx.handle().send(ManagerMessage::Found(port_name)).await?;
        }

        Ok(())
    }

    fn object_name(&self) -> String {
        "/dog/asonix/DeckManager".to_owned()
    }

    #[tracing::instrument]
    async fn ignored(&mut self, port_name: String) -> tokio_actors::Result<()> {
        tracing::debug!("ignored");
        let mut inner = self.inner.write().await;

        if !inner.retrying_ports.remove(&port_name) {
            inner.ignored_ports.insert(port_name);
        }

        Ok(())
    }

    #[tracing::instrument(skip(ctx))]
    async fn connected(
        &mut self,
        deck: Deck,
        ctx: &'_ mut Context<ManagerMessage>,
    ) -> tokio_actors::Result<()> {
        tracing::debug!("connected");

        let was_pending = {
            let mut inner = self.inner.write().await;
            let was_pending = inner.pending_ports.remove(deck.port_name());
            drop(inner);
            was_pending
        };

        if !was_pending {
            return Ok(());
        }

        let port_name = deck.port_name().to_string();
        let object_name = deck.object_name();
        let deck_handle = ctx.spawn_child_with_hooks(
            Actor::new(deck, Deck::turn)
                .on_remove(Deck::on_remove)
                .on_start(Deck::on_start)
                .on_stop(Deck::on_stop),
        );

        let actor_id = deck_handle.actor_id();

        let connected = ConnectedDeck {
            object_name,
            handle: deck_handle,
        };

        {
            let mut inner = self.inner.write().await;

            inner.connected_decks.insert(port_name.clone(), connected);
            inner.children.insert(actor_id, port_name);

            drop(inner);
        }

        // auto generated
        let signal = SignalContext::new(&self.connection, self.object_name())?;
        self.connected_decks_changed(&signal).await?;
        Ok(())
    }

    #[tracing::instrument]
    async fn removed(&mut self, port_name: String) -> tokio_actors::Result<()> {
        tracing::debug!("removed");
        let mut inner = self.inner.write().await;

        inner.pending_ports.remove(&port_name);
        inner.ignored_ports.remove(&port_name);
        if let Some(mut connected) = inner.connected_decks.remove(&port_name) {
            tracing::info!("Sending stop to {}", connected.handle.actor_id());
            connected.handle.stop();
        }

        Ok(())
    }

    #[tracing::instrument(skip(ctx))]
    async fn found(
        &mut self,
        port_name: String,
        ctx: &'_ mut Context<ManagerMessage>,
    ) -> tokio_actors::Result<()> {
        tracing::debug!("found");
        let inner = self.inner.read().await;

        for set in [&inner.pending_ports, &inner.ignored_ports] {
            if set.contains(&port_name) {
                return Ok(());
            }
        }
        if inner.connected_decks.contains_key(&port_name) {
            return Ok(());
        }

        drop(inner);

        let connector = Connector::new(self.connection.clone(), ctx.handle());
        let mut handle = ctx.spawn_child(connector, Connector::turn);
        if let Err(e) = handle.send(port_name.clone()).await {
            handle.stop();
            return Err(e.into());
        }
        self.inner.write().await.pending_ports.insert(port_name);

        Ok(())
    }
}
