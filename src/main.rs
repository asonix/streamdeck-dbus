use tracing_subscriber::{fmt::format::FmtSpan, layer::SubscriberExt, EnvFilter};

mod manager;

use manager::Manager;

#[tokio::main]
async fn main() -> tokio_actors::Result<()> {
    init_tracing()?;

    let mut root = tokio_actors::root();

    Manager::start(&mut root).await?;

    tokio::signal::ctrl_c().await?;

    root.close().await;

    Ok(())
}

fn init_tracing() -> tokio_actors::Result<()> {
    let env_filter = EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));

    let formatter = tracing_subscriber::fmt::layer()
        .pretty()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE);

    let subscriber = tracing_subscriber::Registry::default()
        .with(env_filter)
        .with(formatter)
        .with(tracing_error::ErrorLayer::default());

    tracing::subscriber::set_global_default(subscriber)?;

    Ok(())
}
