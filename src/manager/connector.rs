use crate::manager::{
    deck::{Deck, DeckInfo},
    ManagerMessage,
};
use tokio_actors::{Context, SendHandle};
use zbus::Connection;

#[derive(Clone)]
pub(super) struct Connector {
    connection: Connection,
    parent: SendHandle<ManagerMessage>,
}

impl std::fmt::Debug for Connector {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Connector").finish()
    }
}

impl Connector {
    pub(super) fn new(connection: Connection, parent: SendHandle<ManagerMessage>) -> Self {
        tracing::debug!("new connector");
        Connector { connection, parent }
    }

    #[tracing::instrument(skip(ctx))]
    pub(super) async fn turn(
        &mut self,
        port_name: String,
        ctx: &mut Context<String>,
    ) -> tokio_actors::Result<()> {
        tracing::debug!("Connector");
        ctx.stop();

        match self.connect(port_name.clone()).await {
            Ok(deck) => {
                self.parent.send(ManagerMessage::Connected(deck)).await?;
                Ok(())
            }
            Err(e) => {
                tracing::info!("Error: {}", e);
                tracing::info!("Details: {:?}", e);
                self.parent.send(ManagerMessage::Ignored(port_name)).await?;
                Err(e)
            }
        }
    }

    #[tracing::instrument]
    async fn connect(&mut self, port_name: String) -> tokio_actors::Result<Deck> {
        let mut port = streamdeck_handshake::handshake(port_name.as_str().into()).await?;

        let identity = streamdeck_commands::identity(&mut port).await?;
        let author = streamdeck_commands::author(&mut port).await?;
        let repo = streamdeck_commands::repo(&mut port).await?;
        let version = streamdeck_commands::version(&mut port).await?;

        let info = DeckInfo::new(identity, author, repo, version);

        let deck = Deck::build(
            port,
            self.connection.clone(),
            // TODO: look up device name
            "".to_owned(),
            info,
            port_name,
            self.parent.clone(),
        )
        .await?;

        Ok(deck)
    }
}
