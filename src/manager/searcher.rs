use crate::manager::ManagerMessage;
use std::{
    collections::HashSet,
    path::{Path, PathBuf},
};
use tokio_actors::{Context, SendHandle};

#[derive(Clone)]
pub(super) struct Searcher {
    manager: SendHandle<ManagerMessage>,
    connected: HashSet<String>,
}

impl std::fmt::Debug for Searcher {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Searcher")
            .field("connected", &self.connected)
            .finish()
    }
}

impl Searcher {
    pub(super) fn new(manager: SendHandle<ManagerMessage>) -> Self {
        tracing::debug!("new searcher");
        Searcher {
            manager,
            connected: HashSet::new(),
        }
    }

    #[tracing::instrument(skip(_ctx))]
    pub(super) async fn turn(&mut self, _: (), _ctx: &mut Context<()>) -> tokio_actors::Result<()> {
        let ports = tokio::task::block_in_place(|| tokio_serial::available_ports())?;

        let port_names = ports
            .into_iter()
            .map(|p| p.port_name)
            .collect::<HashSet<String>>();

        for port in port_names.difference(&self.connected) {
            let port = port_name(port);

            self.manager.send(ManagerMessage::Found(port)).await?;
        }

        for port in self.connected.difference(&port_names) {
            let port = port_name(port);

            self.manager
                .send(ManagerMessage::Removed(port.to_owned()))
                .await?;
        }

        self.connected = port_names;

        Ok(())
    }
}

fn port_name(port: &String) -> String {
    if port.starts_with("/sys/class/tty") {
        let port: &Path = port.as_ref();
        let port = port.iter().rev().next().unwrap();
        let device = PathBuf::new().join("/").join("dev").join(port);
        device.to_string_lossy().to_string()
    } else {
        port.to_owned()
    }
}
