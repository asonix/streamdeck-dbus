use crate::manager::ManagerMessage;
use std::sync::Arc;
use tokio::sync::RwLock;
use tokio_actors::{Actor, Context, SendHandle};
use tokio_serial::SerialStream;
use zbus::{Connection, SignalContext};

mod port;

use port::Port;

#[derive(Debug, Clone)]
pub(super) struct DeckInfo {
    id: Id,
    author: String,
    repo: String,
    version: String,
}

#[derive(Clone, Debug)]
pub(super) enum DeckMessage {
    Press(u8),
}

#[derive(Clone)]
pub(super) struct Deck {
    connection: Connection,
    info: DeckInfo,
    port_name: String,
    parent: SendHandle<ManagerMessage>,
    inner: Arc<RwLock<Inner>>,
}

impl std::fmt::Debug for Deck {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Deck")
            .field("info", &self.info)
            .field("port_name", &self.port_name)
            .field("inner", &self.inner)
            .finish()
    }
}

#[derive(Clone, Debug)]
struct Id(Vec<u8>);

struct Inner {
    name: String,
    port: Option<SerialStream>,
    child: Option<usize>,
}

impl std::fmt::Debug for Inner {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Inner").field("name", &self.name).finish()
    }
}

#[zbus::dbus_interface(name = "dog.asonix.DeckManager.Deck1")]
impl Deck {
    #[dbus_interface(property)]
    fn id(&self) -> String {
        self.info.id.to_string()
    }

    #[dbus_interface(property)]
    fn author(&self) -> String {
        self.info.author.clone()
    }

    #[dbus_interface(property)]
    fn repo(&self) -> String {
        self.info.repo.clone()
    }

    #[dbus_interface(property)]
    fn version(&self) -> String {
        self.info.version.clone()
    }

    #[dbus_interface(property)]
    async fn name(&self) -> String {
        self.inner.read().await.name.clone()
    }

    #[dbus_interface(property)]
    async fn set_name(&mut self, name: String) {
        self.inner.write().await.name = name;
    }

    #[dbus_interface(signal)]
    async fn key_pressed(&mut self, ctx: &SignalContext<'_>, key: u8) -> zbus::Result<()>;
}

impl DeckInfo {
    pub(super) fn new(id: Vec<u8>, author: String, repo: String, version: String) -> Self {
        DeckInfo {
            id: Id(id),
            author,
            repo,
            version,
        }
    }
}

impl Deck {
    pub(super) async fn build(
        port: SerialStream,
        connection: Connection,
        name: String,
        info: DeckInfo,
        port_name: String,
        parent: SendHandle<ManagerMessage>,
    ) -> tokio_actors::Result<Self> {
        let deck = Deck {
            connection: connection.clone(),
            info,
            port_name,
            parent,
            inner: Arc::new(RwLock::new(Inner {
                child: None,
                name,
                port: Some(port),
            })),
        };

        connection
            .object_server_mut()
            .await
            .at(deck.object_name(), deck.clone())?;

        tracing::debug!("new deck");
        Ok(deck)
    }

    pub(super) fn object_name(&self) -> String {
        format!("/dog/asonix/DeckManager/{}", self.info.id)
    }

    #[tracing::instrument(skip(_ctx))]
    pub(super) async fn turn(
        &mut self,
        msg: DeckMessage,
        _ctx: &mut Context<DeckMessage>,
    ) -> tokio_actors::Result<()> {
        match msg {
            DeckMessage::Press(key) => self.press(key).await,
        }
    }

    #[tracing::instrument(skip(ctx))]
    pub(super) async fn on_start(
        &mut self,
        ctx: &mut Context<DeckMessage>,
    ) -> tokio_actors::Result<()> {
        let mut inner = self.inner.write().await;

        if let Some(serial) = inner.port.take() {
            let port = Port::new(ctx.handle(), serial);
            let port_handle =
                ctx.spawn_child_with_hooks(Actor::new(port, Port::turn).on_start(Port::on_start));
            inner.child = Some(port_handle.actor_id());
        }

        Ok(())
    }

    #[tracing::instrument(skip(_ctx))]
    pub(super) async fn on_stop(
        &mut self,
        _ctx: &mut Context<DeckMessage>,
    ) -> tokio_actors::Result<()> {
        self.connection
            .object_server_mut()
            .await
            .remove::<Deck, _>(self.object_name())?;

        Ok(())
    }

    #[tracing::instrument(skip(ctx))]
    pub(super) async fn on_remove(
        &mut self,
        child_id: usize,
        ctx: &mut Context<DeckMessage>,
    ) -> tokio_actors::Result<()> {
        let mut inner = self.inner.write().await;

        if let Some(id) = inner.child.take() {
            if id == child_id {
                ctx.stop();
            } else {
                inner.child = Some(id);
            }
        }

        Ok(())
    }

    #[tracing::instrument]
    async fn press(&mut self, key: u8) -> tokio_actors::Result<()> {
        tracing::debug!("{}", key);
        let ctx = SignalContext::new(&self.connection, self.object_name())?;
        self.key_pressed(&ctx, key).await?;
        Ok(())
    }

    pub(super) fn port_name(&self) -> &str {
        &self.port_name
    }
}

impl std::fmt::Display for Id {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for byte in &self.0 {
            write!(f, "{:02X}", byte)?;
        }
        Ok(())
    }
}
