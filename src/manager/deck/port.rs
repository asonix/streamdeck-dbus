use super::DeckMessage;
use tokio::io::AsyncReadExt;
use tokio_actors::{Context, SendHandle};
use tokio_serial::SerialStream;

#[derive(Clone, Debug)]
pub(super) enum PortMessage {
    Start,
}

pub(super) struct Port {
    parent: SendHandle<DeckMessage>,
    serial: SerialStream,
}

impl std::fmt::Debug for Port {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Port").finish()
    }
}

impl Port {
    pub(super) fn new(parent: SendHandle<DeckMessage>, serial: SerialStream) -> Self {
        Port { parent, serial }
    }

    #[tracing::instrument(skip(ctx))]
    pub(super) async fn on_start(
        &mut self,
        ctx: &mut Context<PortMessage>,
    ) -> tokio_actors::Result<()> {
        ctx.stop_on_error();
        ctx.handle().send(PortMessage::Start).await?;

        Ok(())
    }

    #[tracing::instrument(skip(ctx))]
    pub(super) async fn turn(
        &mut self,
        msg: PortMessage,
        ctx: &mut Context<PortMessage>,
    ) -> tokio_actors::Result<()> {
        match msg {
            PortMessage::Start => self.start(ctx).await,
        }
    }

    #[tracing::instrument(skip(ctx))]
    async fn start(&mut self, ctx: &'_ mut Context<PortMessage>) -> tokio_actors::Result<()> {
        ctx.handle().send(PortMessage::Start).await?;

        loop {
            let mut key = [0u8; 1];
            self.serial.read_exact(&mut key).await?;

            self.parent.send(DeckMessage::Press(key[0])).await?;
        }
    }
}
